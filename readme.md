# README #

Introduction:
The code helps easily import data from excel format on a local machine into a dynamodb on aws. It is a cost effective solution for importing small datasets into a dynamodb table when compared to other avenues like using a datapipeline in conjuction with EMR clusters. 

Most of the code was originally taken from a blogpost by Sachin Girdhar.
Link to the post: http://justprogrammng.blogspot.in/2016/08/python-script-to-move-records-from-csv.html

The code in this repo has been tweaked to work with python 3.+

Prerequisites:
1. Python and pip installed on your computer.
2. boto3 installed on your computer. To install boto3 use command: pip install boto3.
3. AWS CLI installed on your computer.
4. Have AWS security credentials for an IAM user having permission to write data into dynamodb.
5. Configure aws credentials on your local machine using aws cli 
6. Create table with column names on dynamo db

Script Arguments:

Mandatory:
csvFile - Path to csv file location
table - Dynamo db table name

Optional:
writeRate - Number of records to write in table per second (default:5)
delimiter - Delimiter for csv records (default=|)
region - Dynamo db region name (default=us-west-2)

Sample:
#With mandatory arguments only:
python [scriptName.py] [.csv] [TableName]
ex: python csvToDynamodb.py abc.csv ABC

#With optional arguments:
python [scriptName.py] [.csv] [TableName] [writeRate] [delimiter] [region]
ex: python python csvToDynamodb.py abc.csv ABC 50 | us-east-1

P.S: Find a smaple csv attached in the repo for reference.